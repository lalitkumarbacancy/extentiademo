/**
 * Main Application file
 * where navigation is start
 * @format
 * @flow strict-local
 */

import React from 'react';
import MainNavigation from './src/navigation/MainNavigation';

export const App: () => React.ReactElement = () => {
  return <MainNavigation />;
};

export default App;
