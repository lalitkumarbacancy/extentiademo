/**
 * test registration screen file unit testing
 */
import React from 'react';
import renderer from 'react-test-renderer';
import {
  render,
  fireEvent,
  RenderAPI,
  waitFor,
} from '@testing-library/react-native';
import {Formik} from 'formik';
import {ReactTestInstance} from 'react-test-renderer';

import RegistrationScreen from '../src/screens/registration/RegistrationScreen';
import FormFields from '../src/screens/registration/FormFields';
import {Strings} from '../src/utility';
import TextInputComponent from '../src/components/TextInputComponent';
import Validator from '../src/screens/registration/validator';

const tree: RenderAPI = render(<RegistrationScreen />);
/**
 * Snap shor testing
 */
test('Registration SnapShot testing', () => {
  expect(tree).toMatchSnapshot();
});

test('Registration component render correctly', () => {
  const renderRegistrationScreen: RenderAPI = render(<RegistrationScreen />);
  expect(renderRegistrationScreen.toJSON()).toBeTruthy();
});

test('Test input Fields', async () => {
  const mockFunction = jest.fn();
  const {
    getByPlaceholderText,
    getByText,
    getByTestId,
    queryAllByText,
  }: RenderAPI = render(<FormFields onSubmit={mockFunction} />);
  //console.log('registration file ', tree);
  const firstName: ReactTestInstance | null = getByTestId('FnameTxtField');
  const lastName: ReactTestInstance | null = getByTestId('LnameTxtField');
  const emailTxt: ReactTestInstance | null = getByTestId('emailTxtField');
  const ageTxt: ReactTestInstance | null = getByTestId('ageTxtField');
  const phoneTxt: ReactTestInstance | null = getByTestId('phoneTxtField');
  const addressTxt: ReactTestInstance | null = getByTestId('addressTxtField');
  const empTxt: ReactTestInstance | null = getByTestId('empTxtField');
  const submitBtn: ReactTestInstance | null = getByTestId('submit');

  await waitFor(() => {
    if (firstName) {
      fireEvent.changeText(firstName, 'Abdulla');
    } else {
      console.log('First Name text response is null');
    }
  });

  await waitFor(() => {
    if (lastName) {
      fireEvent.changeText(lastName, 'Shaikh');
    } else {
      console.log('Last name text response is null');
    }
  });

  await waitFor(() => {
    if (emailTxt) {
      fireEvent.changeText(emailTxt, 'lalit');
    }
  });

  await waitFor(() => {
    if (ageTxt) {
      fireEvent.changeText(ageTxt, '85');
    }
  });

  await waitFor(() => {
    if (phoneTxt) {
      fireEvent.changeText(phoneTxt, '7568804362');
    }
  });

  await waitFor(() => {
    if (addressTxt) {
      fireEvent.changeText(addressTxt, '');
    }
  });

  await waitFor(() => {
    if (empTxt) {
      fireEvent.changeText(empTxt, '');
    }
  });

  await waitFor(() => {
    if (submitBtn) {
      fireEvent.press(submitBtn);
    } else {
      console.log('Submit btn is null....');
    }
  });

  expect(mockFunction).toHaveBeenCalledWith({
    newPassword: '123',
    passwordConfirm: '123',
  });
});

test('Test validator function', async () => {
  const validation = new Validator();
  const isValid = await validation.registrationValidation().isValid({
    firstName: 'lalit',
    email: 'lalit@gmail.com',
    phone: '75688804362',
    age: '74',
    empCode: '12',
  });
  console.log('validator calling --->>>> ', isValid);
  expect(isValid).toBeTruthy();
});
