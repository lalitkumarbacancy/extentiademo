/**
 * test registration screen file unit testing
 */
import React from 'react';
import {
  render,
  fireEvent,
  RenderAPI,
  waitFor,
} from '@testing-library/react-native';
import GeneratePasswordScreen from '../src/screens/generatePasswrod/GeneratePasswordScreen';
import {ReactTestInstance} from 'react-test-renderer';

/**
 * Snap shor testing
 */
test('Registration SnapShot testing', () => {
  const tree: RenderAPI = render(<GeneratePasswordScreen />);
  expect(tree).toMatchSnapshot();
});

test('Test input Fields', async () => {
  const registrationRender: RenderAPI = render(<GeneratePasswordScreen />);
  //console.log('registration file ', tree);
  const passwordTexttxt: ReactTestInstance | null = registrationRender.getByTestId(
    'passwordTxtField',
  );
  const confrimPasswordTxt: ReactTestInstance | null = registrationRender.getByTestId(
    'confirmPasswordTxtField',
  );

  const submitBtn: ReactTestInstance | null = registrationRender.getByTestId(
    'submit',
  );

  // const color = container.querySelector('select[name="color"]');
  // const submit = container.querySelector('button[type="submit"]');
  // const results = container.querySelector('textarea');

  await waitFor(() => {
    if (passwordTexttxt) {
      fireEvent.changeText(passwordTexttxt, 'lalit');
    } else {
      console.log('password text response is null');
    }
  });

  expect(passwordTexttxt.props.value).toBe('lalit');

  // await waitFor(() => {
  //   if (confrimPasswordTxt) {
  //     fireEvent.changeText(confrimPasswordTxt, 'kumar');
  //   } else {
  //     console.log('confirm password text response is null');
  //   }
  // });
  // console.log('confrimPasswordTxt.props --> ', confrimPasswordTxt.props.value);
  // expect(confrimPasswordTxt.props.value).toBe('kumar');

  await waitFor(() => {
    if (submitBtn) {
      fireEvent.press(submitBtn);
    } else {
      console.log('Submit btn is null....');
    }
  });
  //console.log('submitBtn calling --> ', submitBtn);
  await waitFor(() => {
    expect(
      registrationRender.queryAllByText('This field is required'),
    ).toHaveLength(1);
  });
});
