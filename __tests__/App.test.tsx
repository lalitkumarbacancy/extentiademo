/**
 * App.tsx screen testing
 *
 **/
import React from 'react';
import renderer from 'react-test-renderer';
import {render, fireEvent} from '@testing-library/react-native';
import App from '../App';
import RegistrationScreen from '../src/screens/registration/RegistrationScreen';

const tree = render(<RegistrationScreen />);
/**
 * Snapshot testing
 */
test('App SnapShot testing', () => {
  console.log('renders tree ---> ', tree);
  //const tree = renderer.create(<App />);

  expect(tree).toMatchSnapshot();
});

