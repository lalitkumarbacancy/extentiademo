// module.exports = {
//   preset: 'react-native',
//   moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
// };
module.exports = {
  preset: 'react-native',
  transform: {'^.+\\.(ts|tsx)?$': 'ts-jest'},
  transformIgnorePatterns: [
    'node_modules/(?!(@react-native|react-native|react-native-keyboard-aware-scroll-view|react-native-iphone-x-helper|react-native-keyboard-spacer|react-native-material-textfield|react-native-formik|react-native-modalbox|react-native-root-siblings|static-container)/)',
    'mode_modules/.',
  ],
  watchPathIgnorePatterns: ['node_modules'],
};
