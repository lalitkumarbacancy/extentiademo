/**
 * Here we define all strings files
 */

class Strings {
  static REGISTRATION: string = 'Registration';
  static FILL_FOLLOWING: string = 'Fill the following information';
  static SUBMIT: string = 'Submit ';
  static GENERATE_PASSWORD: string = 'Generate Password';
  static FIRST_NAME: string = 'First Name';
  static LAST_NAME: string = 'Last Name';
  static AGE: string = 'Age';
  static EMAIL: string = 'Email';
  static PHONE: string = 'Phone';
  static ADDRESS: string = 'Address';
  static EMPLOYEE_CODE: string = 'Employee Code';
  static FIRST_NAME_KEY: string = 'firstName';
  static LAST_NAME_KEY: string = 'lastName';
  static NAME_KEY: string = 'name';
  static EMAIL_KEY: string = 'email';
  static AGE_KEY: string = 'age';
  static PHONE_KEY: string = 'phone';
  static ADDRESS_KEY: string = 'address';
  static EMP_CODE: string = 'empCode';
  static PASSWORD: string = 'Password';
  static PASSWORD_KEY: string = 'password';
  static CONFIRM_PASSWORD: string = 'Confirm Password';
  static CONFIRM_PASSWORD_KEY: string = 'confirmPassword';
  static FIRST_NAME_MESSAGE: string = 'Please first name?';
  static FIRST_NAME_FULL_MESSAGE: string =
    'Please enter your first name minimum 3 character.';
  static EMAIL_MESSAGE: string = 'Well enter correct email.';
  static PHONE_MESSAGE: string = 'Please phone';
  static PHONE_FULL_MESSAGE: string =
    'Please enter phone number minimum 10 digit';
  static AGE_MESSAGE: string = 'Age is necessary';
  static EMPLOYEE_CODE_MESSAGE: string = 'Employee code is required';
  static PASSWORD_REQUIRED_MESSAGE: string =
    'Well password must have 8 character long with 1 special character 1 numeric and 1 Capital.';
  static PASSWORD_REQUIRED_FULL_MESSAGE: string =
    '8 character long with 1 special character 1 numeric and 1 Capital.';
  static PASSWORD_DO_NOT_MATCH: string = 'Both passwords do not match';
  static THANKS_FOR_REGISTRATION: string = 'Thanks for the registration';
}
export default Strings;
