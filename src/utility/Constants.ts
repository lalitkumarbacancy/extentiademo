/**
 * Here we declared all constants variables
 */
export default class Constants {
  static REGISTRATION: string = 'RegistrationScreen';
  static GENERATE_PASSWORD: string = 'GeneratePasswordScreen';
}
