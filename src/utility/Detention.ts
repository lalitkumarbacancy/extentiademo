/**
 * Get device height and width.
 */
import {Dimensions} from 'react-native';

class Detention {
  /**
   * Method use for get height in percentage
   * @param {*} percentage : height in percentage
   */
  static getHeight = (percentage: number) => {
    const height: number = Dimensions.get('window').height;
    const ratio: number = percentage / 100;
    return ratio * height;
  };

  /**
   * This method use for get the percentage width
   * of the device
   * @param {*} percentage: width in percentage
   */
  static getWidth = (percentage: number) => {
    const width: number = Dimensions.get('window').width;
    const ratio: number = percentage / 100;
    return ratio * width;
  };
}
export default Detention;
