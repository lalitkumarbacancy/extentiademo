/***
 * Here we defined all colors used in this application
 */

class Colors {
  static BLACK: string = '#000000';
  static WHITE: string = '#ffffff';
  static LIGHT_BLUE: string = '#ADD8E6';
  static GRAY: string = '#808080';
  static BLUE: string = '#4169E1';
  static RED: string = '#FF0000';
  static YELLOW: string = '#FFFF00';
  static GREEN: string = '#008000';
}

export default Colors;
