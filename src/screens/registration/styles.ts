import React from 'react';
import {StyleSheet} from 'react-native';

import {Colors} from '../../utility';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  fillTxt: {
    marginVertical: 10,
    marginStart: 10,
  },
  formStyle: {marginHorizontal: 10},
});

export default styles;
