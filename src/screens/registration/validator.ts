/**
 * This class check the validation function
 */
import * as Yup from 'yup';
import {Strings} from '../../utility';

class Validator {
  /**
   * Method check all validation of registration fields
   */
  registrationValidation = () => {
    console.log('registrationValidation calling ...');
    return Yup.object().shape({
      firstName: Yup.string()
        .required(Strings.FIRST_NAME_MESSAGE)
        .min(3, Strings.FIRST_NAME_FULL_MESSAGE),
      email: Yup.string().required(Strings.EMAIL).email(Strings.EMAIL_MESSAGE),
      phone: Yup.string()
        .required(Strings.PHONE_MESSAGE)
        .min(10, Strings.PHONE_FULL_MESSAGE),
      age: Yup.number().required(Strings.AGE_MESSAGE).positive().integer(),
      empCode: Yup.string().required(Strings.EMPLOYEE_CODE_MESSAGE),
    });
  };
}
export default Validator;
