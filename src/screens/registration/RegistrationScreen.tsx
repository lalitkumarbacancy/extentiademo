/**
 * Registration screen
 *
 */
import React, {Component} from 'react';
import {View, StatusBar} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {ToolBar} from '../../components';
import {Strings, Constants} from '../../utility';
import FormFields from './FormFields';
import styles from './styles';

interface RegistrationProps {
  navigation?: {
    navigate?: Function;
  };
}
interface State {
  model: object;
}
interface RegistrationType {
  firstName: string;
  lastName: string;
  email: string;
  age: string;
  phone: string;
  address: string;
  empCode: string;
}
export default class RegistrationScreen extends Component<
  Partial<RegistrationProps>,
  State
> {
  constructor(props: any) {
    super(props);
  }

  /**
   * This Method will call when user
   * press submit
   */
  onSubmitRegistration = (values: RegistrationType) => {
    console.log('onSubmitRegistration calling ', values);
    const {navigation} = this.props;

    if (navigation && navigation.navigate) {
      navigation?.navigate(Constants.GENERATE_PASSWORD);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <ToolBar headerTxt={Strings.REGISTRATION} />
        <KeyboardAwareScrollView style={styles.formStyle}>
          <FormFields onSubmit={this.onSubmitRegistration} />
          <KeyboardSpacer />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
