/**
 * In this file we create the registration
 * Text Input fields
 */
import React from 'react';
import {View, TextInput} from 'react-native';

import {Formik} from 'formik';
import {withNextInputAutoFocusForm} from 'react-native-formik';

import {Button, TextInputComponent} from '../../components';
import {Strings} from '../../utility';
import Validator from './validator';

const validator = new Validator();

interface Props {
  onSubmit: Function;
}
interface State {}
class FormFields extends React.PureComponent<Props, State> {
  static defaultProps: {onSubmit: () => void};
  render() {
    const Form = withNextInputAutoFocusForm(View);

    return (
      <Formik
        onSubmit={() => this.props.onSubmit()}
        validationSchema={validator.registrationValidation()}
        initialValues={{}}
        render={(props) => {
          return (
            <Form>
              <TextInputComponent
                testId={'FnameTxtField'}
                label={Strings.FIRST_NAME}
                name={Strings.FIRST_NAME_KEY}
                errors={props.errors}
                touched={props.touched}
                handleChange={props.handleChange}
                setFieldTouched={props.setFieldTouched}
              />

              <TextInputComponent
                testId={'LnameTxtField'}
                label={Strings.LAST_NAME}
                name={Strings.LAST_NAME_KEY}
                errors={props.errors}
                touched={props.touched}
                handleChange={props.handleChange}
                setFieldTouched={props.setFieldTouched}
              />
              <TextInputComponent
                label={Strings.EMAIL}
                name={Strings.EMAIL_KEY}
                testId={'emailTxtField'}
                errors={props.errors}
                touched={props.touched}
                handleChange={props.handleChange}
                setFieldTouched={props.setFieldTouched}
              />
              <TextInputComponent
                label={Strings.AGE}
                name={Strings.AGE_KEY}
                testId={'ageTxtField'}
                keyboardType="phone-pad"
                maxLength={3}
                errors={props.errors}
                touched={props.touched}
                handleChange={props.handleChange}
                setFieldTouched={props.setFieldTouched}
              />
              <TextInputComponent
                label={Strings.PHONE}
                name={Strings.PHONE_KEY}
                testId={'phoneTxtField'}
                keyboardType="phone-pad"
                maxLength={10}
                errors={props.errors}
                touched={props.touched}
                handleChange={props.handleChange}
                setFieldTouched={props.setFieldTouched}
              />
              <TextInputComponent
                label={Strings.ADDRESS}
                name={Strings.ADDRESS_KEY}
                testId={'addressTxtField'}
                errors={props.errors}
                touched={props.touched}
                handleChange={props.handleChange}
                setFieldTouched={props.setFieldTouched}
              />
              <TextInputComponent
                label={Strings.EMPLOYEE_CODE}
                name={Strings.EMP_CODE}
                testId={'empTxtField'}
                errors={props.errors}
                touched={props.touched}
                handleChange={props.handleChange}
                setFieldTouched={props.setFieldTouched}
              />
              <Button
                testId="submit"
                onClick={props.handleSubmit}
                btnText={Strings.SUBMIT}
              />
            </Form>
          );
        }}
      />
    );
  }
}
FormFields.defaultProps = {
  onSubmit: () => null,
};

export default FormFields;
