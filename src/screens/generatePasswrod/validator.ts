/**
 * This class check the validation function
 */
import * as Yup from 'yup';
import {Strings} from '../../utility';

class Validator {
  /**
   * Method check password validation
   *  minimum 8 length
   *  1 Special charater
   *  1 Numeric value
   */
  passwordValidation = () => {
    return Yup.object().shape({
      password: Yup.string()
        .required(Strings.PASSWORD_REQUIRED_MESSAGE)
        .matches(/^(?=.*?[#?!@$%^&*-])(?=.*?[A-Z])(?=.*?[0-9]).{8,}/, {
          message: Strings.PASSWORD_REQUIRED_FULL_MESSAGE,
        }),
      confirmPassword: Yup.string()
        .required(Strings.PASSWORD_DO_NOT_MATCH)
        .oneOf([Yup.ref('password'), null], Strings.PASSWORD_DO_NOT_MATCH),
    });
  };
}
export default Validator;
