import React from 'react';
import {StyleSheet} from 'react-native';

import {Colors} from '../../utility';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  fillTxt: {
    marginVertical: 10,
    marginStart: 10,
  },
  formViewStyle: {marginHorizontal: 10, marginTop: 20},
  passwordStrengthView: {
    height: 20,
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  passwordStrength: {width: 15, height: 15},
});

export default styles;
