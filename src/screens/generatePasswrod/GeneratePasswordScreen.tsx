/**
 * Registration screen
 *
 */
import React, {Component} from 'react';
import {View, StatusBar, Alert} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Formik, FormikProps} from 'formik';
import {withNextInputAutoFocusForm} from 'react-native-formik';

import {ToolBar, Button, TextInputComponent} from '../../components';
import {Strings, Colors} from '../../utility';
import Validator from './validator';
import styles from './styles';

interface Props {}
interface State {
  passwordStrength: string;
}
const validator = new Validator();

interface MyFormValues {
  password: string;
  confirmPassword: string;
}
export default class GeneratePasswordScreen extends Component<Props, State> {
  constructor(props: any) {
    super(props);
  }
  passwordStrength: string = Colors.WHITE;

  initialValues: MyFormValues = {
    password: '',
    confirmPassword: '',
  };

  /**
   * This Method will call when user
   * press generate password button
   */
  onGeneratePassword = (values: MyFormValues) => {
    console.log('onSubmitRegistration calling ', values);
    Alert.alert('', Strings.THANKS_FOR_REGISTRATION);
  };

  /**
   * Method use to check password
   * strength
   * @param password : password
   */
  checkPasswordStrength = (password: string) => {
    //console.log('check Password colors ', Colors.RED);
    if (password && password.trim().length > 0) {
      if (password.match(/^(?=.*?[#?!@$%^&*-])(?=.*?[A-Z])(?=.*?[0-9]).{8,}/)) {
        //Here we check 1 Special 1 Caps 1 numeric

        this.passwordStrength = Colors.GREEN;
      } else if (password.match(/^(?=.*?[#?!@$%^&*-])(?=.*?[0-9]).{8,}/)) {
        //Here we check 1 special character and 1 numeric character

        this.passwordStrength = Colors.YELLOW;
      } else {
        this.passwordStrength = Colors.RED;
      }
    }

    return password;
  };

  /**
   * Show password strength
   * red : Low
   * yellow: Medium
   * green: Good
   */
  passwordStrengthCheck = () => (
    <View style={styles.passwordStrengthView}>
      <View
        style={[
          styles.passwordStrength,
          {backgroundColor: this.passwordStrength},
        ]}
      />
    </View>
  );

  render() {
    const Form = withNextInputAutoFocusForm(View);

    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <ToolBar headerTxt={Strings.GENERATE_PASSWORD} />
        <KeyboardAwareScrollView style={styles.formViewStyle}>
          <Formik
            onSubmit={this.onGeneratePassword}
            validationSchema={validator.passwordValidation()}
            initialValues={this.initialValues}
            render={(props: FormikProps<MyFormValues>) => {
              return (
                <Form>
                  <TextInputComponent
                    testId={'passwordTxtField'}
                    label={Strings.PASSWORD}
                    name={Strings.PASSWORD_KEY}
                    type={Strings.PASSWORD_KEY}
                    value={this.checkPasswordStrength(props?.values?.password)}
                    errors={props.errors}
                    touched={props.touched}
                    handleChange={props.handleChange}
                    setFieldTouched={props.setFieldTouched}
                  />
                  {this.passwordStrengthCheck()}
                  <TextInputComponent
                    testId={'confirmPasswordTxtField'}
                    label={Strings.CONFIRM_PASSWORD}
                    name={Strings.CONFIRM_PASSWORD_KEY}
                    type={Strings.PASSWORD_KEY}
                    errors={props.errors}
                    touched={props.touched}
                    handleChange={props.handleChange}
                    setFieldTouched={props.setFieldTouched}
                  />
                  <Button
                    testId={'submit'}
                    onClick={props.handleSubmit}
                    btnText={Strings.GENERATE_PASSWORD}
                  />
                </Form>
              );
            }}
          />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
