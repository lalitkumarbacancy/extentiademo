/**
 * In this file we create a custom button
 */
import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import {Colors} from '../utility';

export interface Props {
  onClick: Function;
  btnText: string;
  style?: object;
  textStyle?: object;
  testId?: string;
}
const Button: React.FC<Props> = ({
  onClick,
  btnText,
  style,
  textStyle,
  testId,
}) => {
  return (
    <TouchableOpacity
      style={[styles.buttonStyle, style]}
      testID={testId}
      onPress={() => {
        onClick();
      }}>
      <Text style={[styles.btnTxtStyle, textStyle]}>{btnText}</Text>
    </TouchableOpacity>
  );
};
Button.defaultProps = {
  onClick: () => null,
  btnText: '',
};
export default Button;

const styles = StyleSheet.create({
  buttonStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.BLUE,
    height: 45,
    borderRadius: 10,
    shadowColor: Colors.GRAY,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 0.5,
  },
  btnTxtStyle: {
    fontSize: 16,
    color: Colors.WHITE,
  },
});
