/**
 * Class use for Toolbar show in top of the screen
 */
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {Detention, Colors} from '../utility';

export interface Props {
  headerTxt: string;
}

const ToolBar: React.FC<Props> = ({headerTxt}) => {
  return (
    <View style={styles.toolBarStyle}>
      <Text style={styles.headerTxtStyle}>{headerTxt}</Text>
    </View>
  );
};

export default ToolBar;

const styles = StyleSheet.create({
  toolBarStyle: {
    height: Detention.getHeight(10),
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    paddingTop: 10,
    shadowColor: Colors.GRAY,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 0.5,
  },
  headerTxtStyle: {
    marginStart: 10,
    fontSize: 16,
  },
});
