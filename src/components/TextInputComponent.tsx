import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {
  TextField as MaterialTextInput,
  TextField,
} from 'react-native-material-textfield';
import {withNextInputAutoFocusForm} from 'react-native-formik';
import {Strings, Colors} from '../utility';

interface Props {
  label?: string;
  value?: any;
  name: string;
  type?: string;
  placeHolder?: string;
  touched?: any;
  errors?: any;
  children?: any;
  handleChange: Function;
  setFieldTouched: Function;
  keyboardType?:
    | 'default'
    | 'email-address'
    | 'numeric'
    | 'phone-pad'
    | 'number-pad'
    | 'decimal-pad'
    | 'visible-password'
    | 'ascii-capable'
    | 'numbers-and-punctuation'
    | 'url'
    | 'name-phone-pad'
    | 'twitter'
    | 'web-search';
  maxLength?: number;
  testId?: String;
}
interface State {}
class TextInputComponent extends React.PureComponent<Props, State> {
  textField!: TextField | null;
  focus = () => {
    this.textField?.focus();
  };
  render() {
    const {
      touched,
      errors,
      name,
      type,
      value,
      handleChange,
      setFieldTouched,
      keyboardType,
      testId,
    } = this.props;
    //console.log('errors calling --> ', errors, ' value ', name);
    return (
      <>
        <MaterialTextInput
          ref={(ref) => (this.textField = ref)}
          {...this.props}
          testID={`${testId}`}
          secureTextEntry={type === Strings.PASSWORD_KEY}
          value={value?.[name] ? value?.[name] : ''}
          baseColor={errors?.[name] ? Colors.RED : Colors.GRAY}
          onChangeText={handleChange(`${name}`)}
          onBlur={() => setFieldTouched(`${name}`)}
          keyboardType={keyboardType ? keyboardType : 'default'}
        />
        {touched?.[name] && errors?.[name] && (
          <Text style={styles.errorText}>{errors?.[name]}</Text>
        )}
      </>
    );
  }
}
export default withNextInputAutoFocusForm(TextInputComponent);

const styles = StyleSheet.create({
  errorText: {fontSize: 12, color: Colors.RED},
});
