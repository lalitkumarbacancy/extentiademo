/**
 * In this class we define the
 * main navigation of the application
 */
import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import 'react-native-gesture-handler';

import {Constants} from '../utility';
import RegistrationScreen from '../screens/registration/RegistrationScreen';
import GeneratePasswordScreen from '../screens/generatePasswrod/GeneratePasswordScreen';

const Stack = createStackNavigator();
export interface Props {}
const MainNavigation: React.FC<Props> = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen
          name={Constants.REGISTRATION}
          component={RegistrationScreen}
        />
        <Stack.Screen
          name={Constants.GENERATE_PASSWORD}
          component={GeneratePasswordScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default MainNavigation;
